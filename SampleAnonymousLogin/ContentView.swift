//
//  ContentView.swift
//  SampleAnonymousLogin
//
//  Created by Etsushi Otani on 2021/09/03.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var viewModel = ViewModel()
    
    var body: some View {
        VStack {
            Spacer()
            Text(viewModel.uid)
            Spacer()
            
            TextField("Email", text: $viewModel.email)
                .padding()
                .overlay(Rectangle()
                            .frame(height: 1)
                            .foregroundColor(.gray), alignment: .bottom)
            TextField("PassWord", text: $viewModel.password)
                .padding()
                .overlay(Rectangle()
                            .frame(height: 1)
                            .foregroundColor(Color.gray), alignment: .bottom)
            
            Spacer()
            
            Button(action: {
                viewModel.tappedSignIn()
            }, label: {
                Text("Sign up with Email Address")
                    .foregroundColor(.black)
                    .padding()
                    .frame(maxWidth: .infinity)
            })
            Button(action: {
                viewModel.signOut()
            }, label: {
                Text("Sign out")
                    .foregroundColor(.black)
                    .padding()
                    .frame(maxWidth: .infinity)
            })
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
