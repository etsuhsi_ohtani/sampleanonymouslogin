//
//  ViewModel.swift
//  SampleAnonymousLogin
//
//  Created by Etsushi Otani on 2021/09/03.
//

import Foundation
import FirebaseAuth

class ViewModel: ObservableObject {
    @Published var email = ""
    @Published var password = ""
    @Published var uid = ""
    
    init() {
        signInAmomymous()
    }
    
    func signInAmomymous() {
        Auth.auth().signInAnonymously(completion: { authResult, error in
            guard let user = authResult?.user else {
                return
            }
            self.uid = user.uid
            print("Anonymous Login sccess")
            print(self.uid)
            self.getIDToken()
        })
    }
    
    func tappedSignIn() {
        guard let user = Auth.auth().currentUser else {
            signInAmomymous()
            return
        }
        if user.isAnonymous {
            let credential = EmailAuthProvider.credential(withEmail: email, password: password)
            user.link(with: credential, completion: { authResult, error in
                print("Email Login success")
            })
        } else {
            print("Already Login")
        }
    }
    
    func getIDToken() {
        guard let user = Auth.auth().currentUser else {
            return
        }
        user.getIDToken(completion: { idToken, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            print(idToken)
        })
    }
    
    func signOut() {
        signInAmomymous()
        /*if let user = Auth.auth().currentUser {
            do {
                try Auth.auth().signOut()
                signInAmomymous()
            } catch {
                print("sign out error")
            }
        }*/
    }
}
